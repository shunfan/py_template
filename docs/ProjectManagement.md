# Project Management

> 这个文档将记录一些管理项目的经验作为参考，并持续更新以及分享。

## PI Planning

- GitLab Milestone

  可以通过 Milestone 来定义项目的大方向，从项目开始，到不同阶段的实现，以及上线后的更新。

## Sprint - GitLab Issue

> Sprint 是很多信息技术公司常用的工具，也有很多好处。

### Usage Case

- 通过 Issue，可以在 Git 上维护并布置新的任务，对工程师会非常明了方便。
- Issue 有非常多的功能包括任务的分配，任务预计时间的设置。
- GitLab 会自动记录所有的 Issue 状态。

### Best Practice

- Manager 新建一个 Issue

  <img src="img/create_issue.png" width="640">

  选择一个已有的 Issue 模板，在 Issue 中可以指定具体任务，完成时间，工程师，或者 Issue 类型等等。

- 从 Issue 中 Create PR

  <img src="img/manage_issue.png" width="640">

  工程师被布置一个 Issue 之后，可以直接从 Issue 中创建一个新的 Branch，或者参照[正常步骤](./MergeRequest.md)完成任务并创建 Merge Request。

- 在完成 Merge Request 之后，相应的 Issue 会被关掉，这样就走完了一个新的任务流程。

  <img src="img/close_issue.png" width="640">

## Document

- 可以尝试利用一些 free tool 比如 OneNote，功能丰富并且可以很好的统一管理。(Start with Weekly AI Metting)
