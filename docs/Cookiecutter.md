# Cookiecutter

A command-line utility that creates projects from cookiecutters (project templates), e.g. creating a Python package project from a Python package project template.

# Installation

## Install with Python

```
pip install --user cookiecutter
```

## Install without Python

```
sudo apt-get install cookiecutter
```

如果两种都不适用，我们可以讨论其他的方式。

# Usage

- [Git Documentation](https://github.com/cookiecutter/cookiecutter)

- 在大多数场合，可以使用内部已经创建好的 CookieCutter Template。

  ```
  cookiecutter git@gitlab.com:Leopard-LeiBo/py_template.git
  ```

- 也可以直接在 github 中搜索 Cookiecutter 寻找更多template

- 可以[自行创建](https://cookiecutter.readthedocs.io/en/latest/tutorial2.html) CookieCutter Template，并且上传至 [Leopard-Leibo](https://gitlab.com/Leopard-LeiBo) 以供他人使用。

# Content

## Python Template

> A simple template for general python packages.

    .
    ├── .gitlab                 # Issue/PR templates
    ├── .gitlab-ci.yml          # CI/CI pipelines
    ├── <your_repo>             # Source folder
    │   ├── __init__.py
    │   ├── <package1>
    │   └── <package2>
    ├── tests                   # Test cases
    ├── requirements.txt        # Essentials for local dev
    ├── setup.py                # Future release, work with tox
    ├── tox.ini                 # Automated test/lint framework
    ├── LICENSE
    └── README.md
