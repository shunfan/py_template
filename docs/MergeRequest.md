# Merge Request

- Code Review，检查代码质量、正确性
- 让有经验的 Reviewer 保证代码风格以及框架的统一
- 项目庞大或者多人维护同一个项目时，能够提升开发效率
- 之后可以通过 Merge Request 添加自动化的 Git Pipeline
- 在个人为多个 feature 开发的时候提供便利

# Best Practices

- create a **_new branch_**

  ```sh
  # 确认自己在master branch
  $ git branch
  ...
  * master

  # 如果是多人维护的项目，可能需要更新本地的master
  $ git pull
  ...

  # 创建新的branch
  $ git checkout -b <your_feature_branch_name>
  Switched to a new branch 'your_feature_branch_name'
  ```

- push your changes

  commit 和 push 自己的 code 并且为 `your_feature_branch_name` 创建一个[merge request](https://gitlab.com/Leopard-LeiBo/py_template/-/merge_requests).

  在 push 的时候也许会出现以下错误，按照提示设定 remote upstream 即可。这是一次性的操作，之后可以正常在这个 branch 下直接 `git push`。

  ```sh
  $ git push
  fatal: The current branch merge_request_intro has no upstream branch.
  To push the current branch and set the remote as upstream, use

      git push --set-upstream origin your_feature_branch_name
   or git push -u origin your_feature_branch_name
  ```

- create a **_merge request_**

  在 `description` 下, 选择一个已有的 merge_request_template，并完成基本内容的填写，并添加至少一个 Reviewer。

  <img src="img/merge_request.png" width="640">

- review and complete a merge request

  Reviewer 有时会对 code 提出修改意见，请在修改并且**Resolve thread**之后，完成以及 Merge 进 `master`。

  <img src="img/merge_request_review.png" width="640">

  在完成 `merge request` 时，请选择**Delete source branch**，这一步操作会将不需要的 remote development branch 删除掉，如果有新的内容需要开发，请从 master 中新开一个 branch。

  如下图黄圈所示，我从`commit="update"`创建了一个新的 branch(`branch-a`)，并且做开发以及 push 对应的 change(`commit="add docs"`)。在**Create Merge Request**之后，将进行 review 以及完成 merge request，`branch-a`将会被 merge 进`master`，这样一个开发流程就完成了。

  <img src="img/repo_tree.png" width="640">
