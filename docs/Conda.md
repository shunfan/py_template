# Conda

We strongly recommend to use `conda` to manage Python environments.

We strongly recommend to use different environment for different projects.
This will avoid python package conflicting issues. 

## Installation
I recommend to use miniconda, and can be installed by following instruction [here](https://docs.conda.io/en/latest/miniconda.html)

## Create Conda Environments
```sh
conda info -e  # List all existing Conda Environments, after fresh installation, there should only be base.

conda create -n "myenv" python=3.7.0  # create an enviroment named "myenv", and set python version to be 3.7.0  
```

## Activate and Deactivate Conda Enviroments
```sh
conda activate myenv  # switch to "myenv" enviroment
conda deactivate  # switch to base
```

## Remove Conda Environments
```sh
conda env remove -n myenv  # Remvoe "myenv" enviroment
```
