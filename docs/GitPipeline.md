# GitLab Pipelines

GitLab offers general CI/CD pipelinesfor continuous integration, delivery, and deployment.

# Concepts

- Continuous Integration

  工程师每天尝试 Merge 自己的 Branch 到 Master，这时候就可以用 CI 做一些自动化的检测，这些 CI 可以用来做 test，lint，以及 code convention。通过 CI 对每一个 Merge Request 做检查，从而保证 production 的代码质量以及准确性。

- Continuous Deployment

  与其让工程师在本地做 test、每一次都手动跑一些 script，不如将在 Test -> Library Release / Deployment 全部通过 CI/CD 自动化，在每一次通过 review 并且 Merge 到 master 后自动部署。

# Resources

- [Tutorial](https://docs.gitlab.com/ee/ci/introduction/#continuous-integration)

- [Example](https://docs.gitlab.com/ee/ci/examples/README.html)
