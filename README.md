## Introduction

为什么需要管理软件开发？

- 针对不同 project，选择一个适合的框架，能对多人开发、以及后期维护带来很多帮助。

- [Issue](https://docs.gitlab.com/ee/user/project/issues/), Jira

  这些工具能够系统的管理和维护软件开发者的日常工作，且比较好的结合 Git 这些软件平台，让整个开发流程（布置任务，分配任务，任务跟踪与更新）集合到一个平台上。

- [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/)

  更好的管理代码，通过 Code Review 可以让更有经验的工程师检查并对代码提出意见，从而控制主流的代码质量。

- [Continuous Integration/Deployment](https://docs.gitlab.com/ee/ci/)

  CI 能够通过自己定义统一的 Git 服务器上的流程进行自动化的检查，如 code convention，testing。CD 能够通过定义 Git 服务器的流程进行自动化的 deployment，比如 package release。

## Instruction

- 下载 cookiecutter

  ```
  pip install --user cookiecutter
  ```

- 使用 CookieCutter 产生本地 Repo
  ```
  cookiecutter git@gitlab.com:Leopard-LeiBo/py_template.git
  ```

## Best Practice

### Merge Request

- create a **_new branch_**

  ```
  git checkout -b <your_feature_branch_name>
  ```

  有些情况下，可能需要在本地的 work branch 上更新 master 上的新内容。

  - 首先更新本地的 master branch

    ```
    git fetch origin master:master
    ```

  - 然后将本地的 master 融合进自己的 branch
    ```
    git merge master
    ```

- create a **_merge request_**

  像往常一样 commit 和 push 自己的 code 并且为 `your_feature_branch_name` 创建一个[merge request](https://gitlab.com/Leopard-LeiBo/py_template/-/merge_requests).

  在 `description` 下, 选择一个已有的 merge_request_template，并完成基本内容的填写，并添加至少一个 Reviewer。

  <img src="static/merge_request.png" width="640">

- review and complete a merge request
  Reviewer 有时会对 code 提出修改意见，请在修改并且**Resolve thread**之后，完成以及 Merge 进 `master`。

  <img src="static/merge_request_review.png" width="640">

  在完成 `merge request` 时，请选择**Delete source branch**，这一步操作会将不需要的 remote development branch 删除掉，如果有新的内容需要开发，请从 master 中新开一个 branch。

  如下图黄圈所示，我从`commit="update"`创建了一个新的 branch(`branch-a`)，并且做开发以及 push 对应的 change(`commit="add docs"`)。在**Create Merge Request**之后，将进行 review 以及完成 merge request，`branch-a`将会被 merge 进`master`，这样一个开发流程就完成了。

  <img src="static/repo_tree.png" width="640">

## Helpful Repository Setup

- [Merge Request Settings](https://gitlab.com/Leopard-LeiBo/py_template/edit)

  - `Sqaush Commits`可以将 Merge Request 中的多个 commit 压缩成一个。
  - 可以默认设置`Delete source branch`以便在 Merge 之后删除当前 branch。
  - 设置`All discussions must be resolved`可以强制要求开发成员完成 Reviewer 的要求。

- [Repository Settings](https://gitlab.com/Leopard-LeiBo/py_template/-/settings/repository)
  - `Protected branches` 下可以设置 **Allowed to push** 来保护 `master branch`.
