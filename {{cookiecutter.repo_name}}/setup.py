import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

requirements = []

setuptools.setup(
    python_requires=">=3.6",
    install_requires=requirements,
)