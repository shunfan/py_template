#!/usr/bin/env python

"""Tests for `template` package."""

import unittest

from {{cookiecutter.repo_name}} import func

class TestPackage(unittest.TestCase):
    """Tests for `project` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_reverse_str(self):
        val = "str"
        expected = "rts"
        actual = func.reverse_str(val)
        self.assertEqual(actual, expected)
