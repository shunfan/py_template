def reverse_str(data: str) -> str:
    return data[::-1]
